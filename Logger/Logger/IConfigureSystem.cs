﻿namespace Logger
{
    public interface IConfigureSystem
    {
        bool LogStackFor(LogLevel logLevel);
    }
}