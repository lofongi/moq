﻿namespace Logger
{
    public interface ICreateLogEntryFooter
    {
        void For(LogLevel logLevel);
    }
}