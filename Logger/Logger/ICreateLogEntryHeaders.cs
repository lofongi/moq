﻿namespace Logger
{
    public interface ICreateLogEntryHeaders
    {
        void For(LogLevel logLevel);
    }
}