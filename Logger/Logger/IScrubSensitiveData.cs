﻿namespace Logger
{
    public interface IScrubSensitiveData
    {
        string From(string message);
    }
}