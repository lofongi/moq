﻿

using System;

namespace Logger
{
    public class Logging
    {
        private readonly IScrubSensitiveData scrubSensitiveData;
        private readonly ICreateLogEntryHeaders createLogEntryHeaders;
        private readonly ICreateLogEntryFooter createLogEntryFooter;
        private readonly IConfigureSystem systemConfiguration;
        // comment

        public Logging(IScrubSensitiveData scrubSensitiveData,
                        ICreateLogEntryHeaders createLogEntryHeaders,
                        ICreateLogEntryFooter createLogEntryFooter,
                        IConfigureSystem systemConfiguration)
        {
            this.scrubSensitiveData = scrubSensitiveData;
            this.systemConfiguration = systemConfiguration;
            this.createLogEntryHeaders = createLogEntryHeaders;
            this.createLogEntryFooter = createLogEntryFooter;
        }


        public void CreateEntryFor(string message, LogLevel logLevel)
        {
            createLogEntryHeaders.For(logLevel);

            if (systemConfiguration.LogStackFor(logLevel))
            {
                Console.WriteLine($"Stack /n/n {Environment.StackTrace}");
            }

            Console.WriteLine($"{logLevel} - {scrubSensitiveData.From(message)}");

            createLogEntryFooter.For(logLevel);
        }
    }
}
